class Student
  attr_reader :first_name, :last_name, :courses
  def initialize(fname, lname, courses = [])
    @first_name = fname
    @last_name = lname
    @courses = courses
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    return self if enrolled?(course)
    @courses.each { |c| raise if course.conflicts_with?(c) }
    @courses.push(course)
    course.students.push(self)
  end

  def enrolled?(course)
    @courses.any? { |c| c == course }
  end

  def course_load
    hash = Hash.new(0)
    @courses.each do |c|
      hash[c.department] += c.credits
    end
    hash
  end
end
